(ns read-and-eval.core-test
  (:require [clojure.test :refer :all]
            [read-and-eval.core :refer :all]))

(deftest operand?-test
  (do
    (testing "+" (is (true? (operand? +))))
    (testing "-" (is (true? (operand? -))))
    (testing "*" (is (true? (operand? *))))
    (testing "/" (is (true? (operand? /))))
    (testing "1" (is (false? (operand? 1))))))

(deftest lowest-priority-operand-test
  (do
    (testing "1 + 2 * 3 / 4"
      (is (= + (lowest-priority-operand (list 1 + 2 * 3)))))
    (testing "1 + 2"
      (is (= + (lowest-priority-operand (list 1 + 2)))))
    (testing "1 * 2 + 3"
      (is (= + (lowest-priority-operand (list 1 * 2 + 3)))))
    (testing "1 * 2"
      (is (= * (lowest-priority-operand (list 1 * 2)))))))

(deftest infix-test
  (do
    (testing "1 + 2"
      (is (= (list + 1 2) (infix (list 1 + 2)))))
    (testing "1 + 2 * 3"
      (is (= (list + 1 (list * 2 3)) (infix (list 1 + 2 * 3)))))
    (testing "(eval (infix (list 1 + 2 * 3)))"
      (is (= (eval (infix (list 1 + 2 * 3))) 7)))))