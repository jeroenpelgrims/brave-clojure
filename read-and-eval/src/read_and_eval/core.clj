(ns read-and-eval.core
  (:gen-class))

(def operand-priority
  { + 1
    - 1
    * 2
    / 2})

(defn operand?
  [op]
  (contains? operand-priority op))

(defn priority
  [term]
  (get operand-priority term))

(defn lowest-priority-operand
  [terms]
  (let [operands (filter operand? terms)]
    (reduce
      #(if (< (priority %2) (priority %1)) %2 %1)
      (first operands)
      operands)))

(defn infix
  [terms]
  (if (<= (count terms) 1)
    (first terms)
    (let [op (lowest-priority-operand terms)
          op-index (.indexOf terms op)
          [left right] (split-at op-index terms)
          right (drop 1 right)]
      (list op (infix left) (infix right)))))

(def terms (list 1 + 2))
(infix (list 1 + 2))
