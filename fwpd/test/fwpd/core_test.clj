(ns fwpd.core-test
  (:require [clojure.test :refer :all]
            [fwpd.core :refer :all]
            [clojure.java.io :as io]))

(def data (mapify (parse (slurp filename))))

(deftest test-read-suspects
  (testing "Reads all suspects from file"
    (let [data (read-suspects filename)]
    (do
      (is (= (count data) 7))
      (is (= "Edward Cullen" (:name (first data))))
      (is (= "Carlisle Cullen" (:name (last data))))))))

(deftest test-glitter-filter
  (testing "Glitter 7 returns only Edward"
    (let [filtered-data (glitter-filter 7 data)]
    (do
      (is (= (count filtered-data) 1))
      (is (= "Edward Cullen" (:name (first filtered-data))))))))

(deftest test-glitter-filter-name
  (testing "Glitter 7 returns only Edward"
    (let [filtered-data (glitter-filter-name 7 data)]
    (do
      (is (= (count filtered-data) 1))
      (is (= "Edward Cullen" (first filtered-data)))))))

(deftest test-read-suspects
  (testing "Reads all suspects from file"
    (let [suspects (read-suspects filename)]
      (do
        (is (= 5 (count suspects)))))))

(deftest test-to-csv
  (testing "converting a suspect map to csv"
    (let [test-data1 [{:name "Pol Petrol" :glitter-index 2}]
          test-data2 (conj test-data1 {:name "Test Testensen" :glitter-index 5})]
    (do
      (is (= "Pol Petrol,2" (to-csv test-data1)))
      (is (= "Pol Petrol,2\nTest Testensen,5" (to-csv test-data2)))))))

(deftest test-append-suspect
  (testing "Appends a suspect to a file"
    (let [filename "./test.csv"]
      (try
        (do
          (spit filename "")
          (append-suspect {:name "Test Testensen" :glitter-index 5} filename)
          (is (= "Test Testensen,5" (slurp filename)))
          (append-suspect {:name "Pol Petrol" :glitter-index 2} filename)
          (is (= "Test Testensen,5\nPol Petrol,2" (slurp filename)))
          (append-suspect {:glitter-index 2} filename)
          (is (= "Test Testensen,5\nPol Petrol,2" (slurp filename))))
      (finally
        (io/delete-file filename))))))

(deftest test-validate
  (testing "validate works correctly"
    (is (not (validate validations {:name "" :glitter-index -1})))
    (is (not (validate validations {:name "Pol" :glitter-index -1})))
    (is (not (validate validations {:name "" :glitter-index 10})))
    (is (validate validations {:name "Pol" :glitter-index 10}))))