(ns fwpd.core)
(def filename "suspects.csv")

(def vamp-keys [:name :glitter-index])

(defn str->int
  [str]
  (Integer. str))

(def conversions {:name identity
                  :glitter-index str->int})

(defn convert
  [vamp-key value]
  ((get conversions vamp-key) value))

(defn parse
  [string]
  (let [lines (clojure.string/split string #"\n")
        non-empty-lines (filter #(not (= "" %)) lines)]
    (map #(clojure.string/split % #",") non-empty-lines)))

(defn mapify
  [rows]
  (map
    (fn
      [[name glitter-index]]
      {:name (convert :name name)
       :glitter-index (convert :glitter-index glitter-index)})
      rows))

(defn read-suspects
  [filename]
  (into [] (mapify (parse (slurp filename)))))

(defn to-csv
  [suspects]
  (let [csv-suspects (map #(str (:name %) "," (:glitter-index %)) suspects)]
  (clojure.string/join "\n" csv-suspects)))

(def validations {:name #(and (not= % "") (not= % nil))
                  :glitter-index #(>= % 0)})

(defn validate
  [validations record]
  (every? (fn [[key validation-fn]] (validation-fn (get record key))) validations))

(defn append-suspect
  [suspect filename]
  (let [suspects (read-suspects filename)
        new-suspects (conj suspects suspect)]
    (if (validate validations suspect)
      (spit filename (to-csv new-suspects))
      nil)))

(defn glitter-filter
  [minimum-glitter records]
  (filter #(>= (:glitter-index %) minimum-glitter) records))

(defn glitter-filter-name
  [minimum-glitter records]
  (map :name (glitter-filter minimum-glitter records)))


(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println (glitter-filter 7 (read-suspects filename))))
