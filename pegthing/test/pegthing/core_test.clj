(ns pegthing.core-test
  (:require [clojure.test :refer :all]
            [pegthing.core :refer :all]))

(deftest test-tri
  (testing "Testing if tri sequence is correct"
    (do
      (is (= 1 (nth tri 0)))
      (is (= 3 (nth tri 1)))
      (is (= 6 (nth tri 2)))
      (is (= 10 (nth tri 3))))))

(deftest test-triangular?
  (testing "Testing if triangular? checks correctly"
    (do
      (is (triangular? 1))
      (is (triangular? 3))
      (is (triangular? 6))
      (is (triangular? 10))
      (is (not (triangular? 2)))
      (is (not (triangular? 4)))
      (is (not (triangular? 5)))
      (is (not (triangular? 7)))
      (is (not (triangular? 8)))
      (is (not (triangular? 9)))
      (is (not (triangular? 11))))))

(deftest test-row-tri
  (testing "Returns the last element of the nth row"
    (do
      (is (= 1 (row-tri 1)))
      (is (= 3 (row-tri 2)))
      (is (= 6 (row-tri 3)))
      (is (= 10 (row-tri 4))))))

(deftest test-row-num
  (testing "Returns the row the position belongs to"
    (do
      (is (= 1 (row-num 1)))
      (is (= 2 (row-num 2)))
      (is (= 2 (row-num 3)))
      (is (= 3 (row-num 4)))
      (is (= 3 (row-num 5)))
      (is (= 3 (row-num 6)))
      (is (= 4 (row-num 8))))))

(deftest test-connect
  (testing "Forms a connection between 2 positions"
    (let [solution {1 {:connections {4 2}}
                    4 {:connections {1 2}}}]
      (is (= (connect {} 15 1 2 4) solution)))))

(deftest test-connect-down-left
  (testing "connect-down-left"
    (let [solution {1 {:connections {4 2}}
                    4 {:connections {1 2}}}]
      (is (= (connect-down-left {} 15 1) solution)))))

(deftest test-connect-down-right
  (testing "connect-down-right"
    (let [solution {3  {:connections {10 6}}
                    10 {:connections {3 6}}}]
      (is (= (connect-down-right {} 15 3) solution)))))

(deftest test-add-pos
  (testing "add-pos"
    (let [solution {1 {:connections {6 3, 4 2}, :pegged true}
                    4 {:connections {1 2}}
                    6 {:connections {1 3}}}]
      (is (= (add-pos {} 15 1) solution)))))

(deftest test-new-board
  (testing "new-board"
    (let [solution {1  {:pegged true, :connections {6 3, 4 2}},
                    2  {:pegged true, :connections {9 5, 7 4}},
                    3  {:pegged true, :connections {10 6, 8 5}},
                    4  {:pegged true, :connections {13 8, 11 7, 6 5, 1 2}},
                    5  {:pegged true, :connections {14 9, 12 8}},
                    6  {:pegged true, :connections {15 10, 13 9, 4 5, 1 3}},
                    7  {:pegged true, :connections {9 8, 2 4}},
                    8  {:pegged true, :connections {10 9, 3 5}},
                    9  {:pegged true, :connections {7 8, 2 5}},
                    10 {:pegged true, :connections {8 9, 3 6}},
                    11 {:pegged true, :connections {13 12, 4 7}},
                    12 {:pegged true, :connections {14 13, 5 8}},
                    13 {:pegged true, :connections {15 14, 11 12, 6 9, 4 8}},
                    14 {:pegged true, :connections {12 13, 5 9}},
                    15 {:pegged true, :connections {13 14, 6 10}},
                    :rows 5}]
      (is (= (new-board 5) solution)))))

(deftest test-valid-moves
  (testing "valid-moves"
    (let [my-board (assoc-in (new-board 5) [4 :pegged] false)]
      (do
        (is (= {4 2} (valid-moves my-board 1)))
        (is (= {4 5} (valid-moves my-board 6)))
        (is (= {4 7} (valid-moves my-board 11)))
        (is (= {} (valid-moves my-board 5)))
        (is (= {} (valid-moves my-board 8)))))))

(deftest test-valid-move?
  (testing "valid-move?"
    (let [my-board (assoc-in (new-board 5) [4 :pegged] false)]
      (do
        (is (= nil (valid-move? my-board 8 4)))
        (is (= 2 (valid-move? my-board 1 4)))))))

(deftest test-row-positions
  (testing "row-positions"
      (do
        (is (= [1] (row-positions 1)))
        (is (= [2 3] (row-positions 2)))
        (is (= [7 8 9 10] (row-positions 4))))))
        